import { combineReducers } from "redux";
import FruitReducer from "./Component/FruitsComponent/FruitReducer";
import AnimalReducer from "./Component/AnimalsComponent/AnimalReducer";

export const CombineReducer = combineReducers({
  fruitCombineData: FruitReducer,
  animalCombineData: AnimalReducer
});
