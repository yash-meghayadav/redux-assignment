import React from "react";
import Modal from "./AddModel";
import "../../node_modules/bootstrap/dist/css/bootstrap.min.css";
import "../styles.css";

class ModelComponent extends React.Component {
  constructor(props) {
    console.log("Initial state");
    super(props);
    this.state = {
      modal: false,
      fullSizeImageUrl: this.props.url
    };
  }

  modalClose() {
    this.setState({
      modal: false
    });
  }
  componentWillMount() {
    console.log("will mount");
    this.setState({
      modal: true
    });
  }

  render() {
    return (
      <div>
        <Modal show={this.state.modal} handleClose={e => this.modalClose(e)}>
          <div className="form-group">
            <img src={this.state.fullSizeImageUrl}></img>
          </div>
        </Modal>
      </div>
    );
  }
}
export default ModelComponent;
