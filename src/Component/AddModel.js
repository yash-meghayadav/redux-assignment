import React from "react";
import "../styles.css";
const AddModal = ({ handleClose, show, children }) => {
  const showHideClassName = show ? "modal d-block" : "modal d-none";

  return (
    <div className={showHideClassName}>
      <div className="modal-container">
        <button style={{ float: "right" }} onClick={handleClose}>
          <i className="fa fa-close"></i>
        </button>
        {children}
      </div>
    </div>
  );
};

export default AddModal;
