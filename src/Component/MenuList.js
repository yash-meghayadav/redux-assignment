import React, { Component } from "react";
import { Switch, Route, Redirect } from "react-router-dom";

import AnimalContainer from "./AnimalsComponent/AnimalContainer";
import FruitContainer from "./FruitsComponent/FruitContainer";

class MenuList extends Component {
  render() {
    return (
      <Switch>
        <Redirect exact from="/" to="/FruitContainer" />
        <Route render={props => <AnimalContainer />} path="/AnimalContainer" />
        <Route render={props => <FruitContainer />} path="/FruitContainer" />
      </Switch>
    );
  }
}
export default MenuList;
