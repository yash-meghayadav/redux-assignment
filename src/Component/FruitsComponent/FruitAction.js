import axios from "axios";
export const FruitAction = data => dispatch => {
  axios
    .get("http://styleguide.effectivedigital.com/interview/api/fruitveg")
    .then(response => {
      data = response.data;
      dispatch({
        type: "ADD_FRUIT",
        payload: data
      });
    });
};
