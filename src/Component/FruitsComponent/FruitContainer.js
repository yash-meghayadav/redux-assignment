import Fruit from "./Fruits";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { FruitAction } from "./FruitAction";

const mapStateToProps = state => ({
  fruitData: state.fruitCombineData.fruitResponse
});

const mapDispatchToPros = dispatch =>
  bindActionCreators({ FruitAction }, dispatch);

const FruitContainer = connect(
  mapStateToProps,
  mapDispatchToPros
)(Fruit);

export default FruitContainer;
