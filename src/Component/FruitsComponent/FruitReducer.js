const initialState = {
  listFru: {
    id: 3
  }
};

const FruitReducer = (state = [], { type = "", payload = {} }) => {
  switch (type) {
    case "ADD_FRUIT":
      return {
        ...state,
        fruitResponse: payload
      };

    default:
      return state;
  }
};
export default FruitReducer;
