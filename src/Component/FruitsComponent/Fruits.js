import React from "react";
import ReactDOM from "react-dom";
import ModelComponent from "../ModelComponent";
import "../../styles.css";

const divStyle = {
  display: "flex",
  alignItems: "center"
};

class Fruits extends React.Component {
  state = {
    fruits: []
  };

  componentDidMount() {
    this.props.FruitAction(this.state.fruits);
  }

  onClickOpenModel = url => {
    ReactDOM.unmountComponentAtNode(
      document.getElementById("openModelForFruitImg")
    );
    ReactDOM.render(
      <ModelComponent url={url} />,
      document.getElementById("openModelForFruitImg")
    );
  };
  /*

              
             
<div>{item.Description}</div>
               <div >{item.Id} </div>
               <div >{item.Family}</div>
              <div >{item.Genus}</div>
              */
  render() {
    const data = this.props.fruitData != undefined ? this.props.fruitData : [];
    return (
      <>
        {data.map((item, i) => (
          <>
            <div className="container" style={{ maxWidth: "300px" }}>
              <div className="desc">{item.Title}</div>

              <img
                className="containerimg"
                src={item.ImageURLs.Thumb}
                onClick={this.onClickOpenModel.bind(
                  this,
                  item.ImageURLs.FullSize
                )}
              ></img>
              <div id="openModelForFruitImg"></div>
            </div>
          </>
        ))}
      </>
    );
  }
}
export default Fruits;
