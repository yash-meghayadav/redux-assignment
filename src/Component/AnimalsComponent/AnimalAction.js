import axios from "axios";

export const AnimalAction = data => dispatch => {
  axios
    .get("http://styleguide.effectivedigital.com/interview/api/animals")
    .then(response => {
      data = response.data;
      dispatch({
        type: "ADD_ANIMAL",
        payload: data
      });
    });
};
