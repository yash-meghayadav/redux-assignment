const AnimalReducer = (state = {}, { type = "", payload = {} }) => {
  switch (type) {
    case "ADD_ANIMAL":
      return {
        ...state,
        animalResponse: payload
      };

    default:
      return state;
  }
};
export default AnimalReducer;
