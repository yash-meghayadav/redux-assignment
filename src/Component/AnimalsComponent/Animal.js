import React from "react";
import ReactDOM from "react-dom";
import ModelComponent from "../ModelComponent";
import "../../styles.css";

class Animal extends React.Component {
  state = {
    animals: []
  };
  componentDidMount() {
    this.props.AnimalAction(this.state.animals);
  }

  onClickOpenModel = url => {
    ReactDOM.unmountComponentAtNode(
      document.getElementById("openModelForAnimalImg")
    );

    ReactDOM.render(
      <ModelComponent url={url} />,
      document.getElementById("openModelForAnimalImg")
    );
  };
  render() {
    const data =
      this.props.animalData != undefined ? this.props.animalData : [];
    return (
      <>
        {data.map((item, i) => (
          <>
            <div className="container" style={{ maxWidth: "300px" }}>
              <div className="desc">{item.Title}</div>

              <img
                className="containerimg"
                src={item.ImageURLs.Thumb}
                onClick={this.onClickOpenModel.bind(
                  this,
                  item.ImageURLs.FullSize
                )}
              ></img>
              <div id="openModelForAnimalImg"></div>
            </div>
          </>
        ))}
      </>
    );
  }
}
export default Animal;
