import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { AnimalAction } from "./AnimalAction";
import Animal from "./Animal";

const mapStateToProps = state => ({
  animalData: state.animalCombineData.animalResponse
  //fruitCombineData: state.fruitCombineData
});

const mapDispatchToPros = dispatch =>
  bindActionCreators({ AnimalAction }, dispatch);

const AnimalContainer = connect(
  mapStateToProps,
  mapDispatchToPros
)(Animal);

export default AnimalContainer;
