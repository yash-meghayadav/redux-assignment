import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import { Provider } from "react-redux";
import { applyMiddleware, createStore } from "redux";
import thunkMiddeleware from "redux-thunk";
import AnimalContainer from "./Component/AnimalsComponent/AnimalContainer";
import FruitContainer from "./Component/FruitsComponent/FruitContainer";
import { CombineReducer } from "./CombineReducer";
import { createLogger } from "redux-logger";

const logger = createLogger();

const store = createStore(
  CombineReducer,
  applyMiddleware(thunkMiddeleware, logger)
);
ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root")
);

//ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
