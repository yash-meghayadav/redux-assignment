import React from "react";
import logo from "./logo.svg";
import "./App.css";
import MenuList from "./Component/MenuList";
import { BrowserRouter as Router } from "react-router-dom";
import Dashboard from "./Dashboard";
import "./styles.css";

function App() {
  return (
    <>
      <Router>
        <Dashboard />
        <div className="divPadding">
          <MenuList />
        </div>
      </Router>
    </>
  );
}

/*
class App extends React.Component {
  state = {};
  componentDidMount() {}
  render() {
    console.log(this.props);
    return (
      <div className="App">
        <AnimalContainer />
        <FruitContainer />
      </div>
    );
  }
}

*/

export default App;
