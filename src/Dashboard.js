import React from "react";
import { Link } from "react-router-dom";
import AnimalContainer from "./Component/AnimalsComponent/AnimalContainer";
import FruitContainer from "./Component/FruitsComponent/FruitContainer";

const Dashboard = () => {
  return (
    <div>
      <html lang="en">
        <body>
          <div className="topnav">
            <Link to="/FruitContainer">Fruit</Link>
            <Link to="/AnimalContainer">Animal</Link>
          </div>
        </body>
      </html>
    </div>
  );
};

export default Dashboard;
